const BUILD = 0;
const API_BUILD = 1.1;
window.developer = function(){
    console.info(`Build Website ${BUILD}`);
    console.info(`Build API ${API_BUILD}`)
    this.console.info(`-> See /doc/api/${API_BUILD}.md`)
}

console.info("developer()")